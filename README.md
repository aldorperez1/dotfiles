# aldorperez's dotfiles


## Usage

**To make changes**

```bash
chezmoi edit ~/.config/zsh/.zshrc
```

This will open the chezmoi file instead of the installed file

## If your target file has changed and is not in sync with chezmoi
 
See the difference between your configuration and what is currently on your machine
```bash
chezmoi diff
```
If you want to try to resolve the conflicts using a diff tool

```bash
chezmoi merge ~/.config/zsh/.zshrc
```

This will open up the diff of your choice (in my case Beyond Compare) and will have you resolve the difference. 

If you just want to take the change made on the machine, then

```bash
chezmoi re-add ~/.hammerspoon/init.lua
```

## Get new changes that are on git

```bash
chezmoi update
```

## Change the init script for chezmoi
The init script has all the configurations for this machine, which all configuration is based on. It is located at `~/.config/chezmoi/chezmoi.toml`

```bash
chezmoi init
```
## Change to the repo containing dotfiles

```bash
chezmoi cd
```

## Refresh Externals

Source: [Include files from elsewhere - chezmoi](https://www.chezmoi.io/user-guide/include-files-from-elsewhere/)

```bash
chezmoi --refresh-externals apply
```

## Add a new config file

```bash
chezmoi add ~/.config/zsh/zshrc
```