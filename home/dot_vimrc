set nocompatible      "Don't do weird stuff for vi

" Check to see if vim plug is installed, if not it installs it
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'tpope/vim-sensible'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'preservim/nerdtree'
Plug 'micha/vim-colors-solarized'
call plug#end()

"*****************************************************************************
"" Basic Setup
"*****************************************************************************"
"""""""""""""""""""""""""" Most important settings """"""""""""""""""""""""" {{{
filetype plugin on    "allow for autocmds to be run based on filetype
set number            "turn on line numbers in the left columns
set formatoptions+=j  "Delete comment character when joining commented lines

" Enable syntax, and set a colorscheme. Don't do this twice
"if !exists("colors_name")
"  syntax enable       "allow for syntax highlighting and indenting
"  colorscheme desert  "pretty colors
"endif

" briefly show the matching bracket {[( when typing )]}
set showmatch matchtime=3
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

" Turn on syntax
syntax on

" Show number lines
set number

"" Tabs. May be overridden by autocmd rules
set expandtab " When enabled, makes spaces when tab is pressed
set tabstop=4 " how many spaces are occuppied by a tab character '\t'
set softtabstop=4 " Referred to for the tab and backspace key. how much whitespace should be interted when the tab key is pressed? and how much whitespace should be removed when the backspace is pressed"
set shiftwidth=4 " Used for levels of indentation '>>'

" Indent new line the same amount as the line just typed
set autoindent

"" Map leader to ,
let mapleader=','

"" Enable hidden buffers to allow you to work on multiple files without having
" to save everytime
set hidden

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

set fileformats=unix,dos,mac

if exists('$SHELL')
    set shell=$SHELL
else
    set shell=/bin/sh
endif

" Highlight search results
set hlsearch

" iterm
set t_Co=16

if has("gui_running")
  set background=dark
  colorscheme solarized
	set guifont=Menlo\ Regular:h15
else
	colorscheme default
endif

set mouse=a

"*****************************************************************************
"" Plugin Configs
"*****************************************************************************

" vim-airline
let g:airline_theme = 'powerlineish'
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline_skip_empty_sections = 1

"" NERDTree configuration
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
let g:NERDTreeWinSize = 50
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
nnoremap <silent><leader>v :NERDTreeFind<CR>
nnoremap <leader>f :NERDTreeToggle<CR>
"nmap <C-n> :NERDTreeToggle<CR>

"*****************************************************************************
"" Mappings
"*****************************************************************************
"" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>
set pastetoggle=<F4>
