#######################################
# Gives the name of an ec2 instance rather than the id
# Globals:
#   None
# Arguments:
#   Instance-id that you want to know the name of
#   Profile that you want to use
# Outputs:
#   Writes the names of the ec2 instances to stdout
####################################### 
function ec2_id_to_name { 
  if [ -z "$2" ]; then
      aws ec2 describe-instances --filters --filters Name=instance-id,Values=
      "$1" --query "Reservations[0].Instances[0].Tags[?Key=='Name'].Value|[0]"
  else
      aws ec2 describe-instances --filters --filters Name=instance-id,Values=
      "$1" --query "Reservations[0].Instances[0].Tags[?Key=='Name'].Value|[0]"
      --profile "$2"
  fi
}