
hs.application.enableSpotlightForNameSearches(true)


--------------------------------------------------------------------------------
-- Hot key bindings
--------------------------------------------------------------------------------
-- This section sets up hot key bindings for various applications.


-- It receives a string and returns a function that launches or focuses the application with that name
function launchOrFocus(app)
    return function()
        hs.application.launchOrFocus(app)
    end
end

-- It receives a string and returns a function that executes the command
function execute(command)
    return function()
        hs.execute(command)
    end
end

modifier_chord_hyper = {'alt', 'shift', 'ctrl', 'cmd'}
local bindings = {
    [modifier_chord_hyper] = {
        -- a
        b = launchOrFocus('Firefox'),               -- B - Browser
        c = launchOrFocus('Slack'),                 -- C - Chat
        -- d
        e = launchOrFocus('Visual Studio Code'),    -- E - Editor
        f = launchOrFocus('Finder'),                -- F - Files
        g = launchOrFocus('Gitkraken'),             -- G - Git client
        -- h
        i = launchOrFocus('Microsoft Outlook'),     -- I - Inbox
        -- j -- MUTE TEAMS
        -- k
        l = launchOrFocus('Omnifocus'),             -- L - todo List
        m = launchOrFocus('Spotify'),               -- M - Music
        n = launchOrFocus('Obsidian'),              -- N - Notes
        -- o
        p = launchOrFocus('1password'),             -- P - Password Manager
        -- q
        -- r
        s = execute('screencapture -Jwindow -i ~/Screenshots/$(date +%Y-%m-%d-%H-%M-%S).png'), -- S - Screenshot
        t = launchOrFocus('iTerm'),                 -- T - Terminal
        -- u
        v = launchOrFocus('Microsoft Teams')        -- Video Conference
        -- w
        -- x
        -- y
        -- z
    }
}
for modifier, keyActions in pairs(bindings) do
    for key, action in pairs(keyActions) do
        hs.hotkey.bind(modifier, tostring(key), action)
    end
end

hs.hotkey.bind(modifier_chord_hyper, ";", launchOrFocus('System Settings'))

--------------------------------------------------------------------------------
-- MicMute
--------------------------------------------------------------------------------
local mm = hs.loadSpoon("MicMute")

local mmkeybind = {"toggle"}
mmkeybind.toggle = {[1]={"cmd", "option", "shift", "ctrl"}, [2]="j"}
mm:bindHotkeys(mmkeybind, 1)

-- local mmfunctionkeybind = {"toggle"}
-- mmfunctionkeybind.toggle = {[1]={}, [2]="f13"}
-- mm:bindHotkeys(mmfunctionkeybind, 1)

-- Print a list of all applications and get their bundle ID
-- hs.fnutils.each(hs.application.runningApplications(), function(app) print(app:title(), app:bundleID()) end)

--------------------------------------------------------------------------------
-- Add a an item to clear the console
--------------------------------------------------------------------------------
hs.console.toolbar():addItems(
    {
        id = "clearConsole",
        image   = hs.image.imageFromName("NSTrashFull"),
        fn      = function(...) hs.console.clearConsole() end,
        label   = "Clear",
        tooltip = "Clear Console",
    }
):insertItem("clearConsole", #hs.console.toolbar():visibleItems() + 1)

--------------------------------------------------------------------------------
-- Example of how to use the logger
--------------------------------------------------------------------------------

-- local log = hs.logger.new("mine", "debug")
-- log.d("HI")