## Markdown

yzhang.markdown-all-in-one #personal #work
bierner.markdown-mermaid #personal #work 
jebbs.markdown-extended #work
telesoho.vscode-markdown-paste-image #work
Sycl.markdown-command-runner #work

## Diagraming

hediet.vscode-drawio #personal
nopeslide.vscode-drawio-plugin-mermaid #personal # work

## Go

golang.go #work #personal
jinliming2.vscode-go-template #personal #work

## K8s

ms-kubernetes-tools.vscode-kubernetes-tools #personal

## Python Dev

ms-python.python #personal
ms-python.vscode-pylance #personal
ms-python.isort #work

### Jupyter

ms-toolsai.jupyter #personal
ms-toolsai.vscode-jupyter-cell-tags #personal
ms-toolsai.vscode-jupyter-slideshow #personal

## Packer

4ops.packer #work

## Terraform

hashicorp.terraform #work

## YAML & Ansible

redhat.vscode-yaml

## Formating

esbenp.prettier-vscode #work

## GIT

eamodio.gitlens #work

## Theme

zhuangtongfa.material-theme #personal
akamud.vscode-theme-onelight #work
GitHub.github-vscode-theme #work
enkia.tokyo-night #work

## Icons

vscode-icons-team.vscode-icons #work
PKief.material-icon-theme

## Latex

mathematic.vscode-latex

## UTILS

ms-vscode-remote.remote-ssh
oderwat.indent-rainbow #work
neptunedesign.vs-sequential-number #personal
chrisdias.vscode-opennewinstance #work
fabiospampinato.vscode-open-in-github #work
chintans98.markdown-jira #work

## Productivity

Gruntfuggly.todo-tree #work
wayou.vscode-todo-highlight #work
davraamides.todotxt-mode #work
hanskre.markdown-execute #work
